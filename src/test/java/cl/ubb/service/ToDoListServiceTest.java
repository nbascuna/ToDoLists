package cl.ubb.service;
import cl.ubb.dao.ToDoDao;
import cl.ubb.model.ToDo;
import cl.ubb.service.ToDoService;
import junit.framework.Assert;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyBoolean;

@RunWith(MockitoJUnitRunner.class)
public class ToDoListServiceTest {
	
	//clase a la cual se realizar mock
	@Mock
	private ToDoDao ToDoDao;
	
	@InjectMocks
	private ToDoService ToDoService; 
	

	@Test
	public void CrearToDoYGuardarlo() {
		
		//arrange
		ToDo ToDo = new ToDo(); 
		ToDo ToDoGuardado = new ToDo();
		
		//act
		when(ToDoDao.save(ToDo)).thenReturn(ToDo);
		ToDoGuardado = ToDoService.guardarToDo(ToDo);
		
		//assert
		Assert.assertNotNull(ToDoGuardado);
		Assert.assertEquals(ToDo,ToDoGuardado);
	}
	
	@Test
	public void ObtenerToDoPorId() {
		
		//arrange
		ToDo ToDo = new ToDo();
		ToDo ToDoRetornado = new ToDo();
		
		//act
		when(ToDoDao.findOne(anyLong())).thenReturn(ToDo);
		ToDoRetornado = ToDoService.obtenerToDoId(ToDo.getId());
		
		//assert
		Assert.assertNotNull(ToDoRetornado);
		Assert.assertEquals(ToDoRetornado, ToDo);
	}
	
	@Test
	public void ObtenerToDoPorCategoria() {
		
		//arrange
		ToDo ToDo = new ToDo();
		ToDo ToDoRetornado = new ToDo();
		
		//act
		when(ToDoDao.findByCategoria(anyString())).thenReturn(ToDo);
		ToDoRetornado = ToDoService.obtenerToDoCategoria(ToDo.getCategoria());
		
		//assert
		Assert.assertNotNull(ToDoRetornado);
		Assert.assertEquals(ToDoRetornado, ToDo);
	}
	
	@Test
	public void ObtenerTodosLosToDo() {
		
		//arrange
		List<ToDo> misToDo = new ArrayList<ToDo>(); 
		List<ToDo> resultado = new ArrayList<ToDo>();
		
		//act
		when(ToDoDao.findAll()).thenReturn(misToDo);
		resultado = ToDoService.obtenerTodos();
		
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(misToDo, resultado);
	}
	
	@Test
	public void ObtenerToDoPorEstado() {
		
		//arrange
		ToDo ToDo = new ToDo();
		ToDo ToDoRetornado = new ToDo();
		
		//act
		when(ToDoDao.findByEstado(anyBoolean())).thenReturn(ToDo);
		ToDoRetornado = ToDoService.obtenerToDoEstado(ToDo.getEstado());
		
		//assert
		Assert.assertNotNull(ToDoRetornado);
		Assert.assertEquals(ToDoRetornado, ToDo);
	}
	
	@Test
	public void EliminarToDo() {
		
		//arrange
		ToDo ToDo = new ToDo(); 
		ToDo ToDoEliminado = new ToDo();
		
		//act
		when(ToDoDao.findOne(anyLong())).thenReturn(ToDo);
		ToDoEliminado = ToDoService.eliminarToDo(ToDo);
		
		//assert
		Assert.assertNotNull(ToDoEliminado);
		Assert.assertEquals(ToDo,ToDoEliminado);
	}
}