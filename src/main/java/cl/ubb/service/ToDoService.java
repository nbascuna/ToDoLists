package cl.ubb.service;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import cl.ubb.dao.ToDoDao;
import cl.ubb.model.ToDo;

public class ToDoService {
	//inyeccion clase
		@Autowired
		private  ToDoDao ToDoDao;
		
		public  ToDo guardarToDo(ToDo ToDo) {
			return ToDoDao.save(ToDo);
		}
		
		public ToDo obtenerToDoId(long id) {
			// TODO Auto-generated method stub
			ToDo ToDo = new ToDo();
			ToDo = ToDoDao.findOne(id);
			return ToDo;
		}
		
		public ToDo obtenerToDoCategoria(String categoria) {
			// TODO Auto-generated method stub
			ToDo ToDo = new ToDo();
			ToDo = ToDoDao.findByCategoria(categoria);
			return ToDo;
		}
		
		public List<ToDo> obtenerTodos() {
			// TODO Auto-generated method stub
			List<ToDo> misToDo = new ArrayList<ToDo>();
			misToDo = (ArrayList<ToDo>) ToDoDao.findAll();
			return misToDo;
		}
		
		public ToDo obtenerToDoEstado(boolean estado) {
			// TODO Auto-generated method stub
			ToDo ToDo = new ToDo();
			ToDo = ToDoDao.findByEstado(estado);
			return ToDo;
		}
		
		public ToDo eliminarToDo(ToDo ToDo){
			List<ToDo> eliminar = new ArrayList<ToDo>();
			eliminar.remove(ToDo);
			return ToDo;
		}
}